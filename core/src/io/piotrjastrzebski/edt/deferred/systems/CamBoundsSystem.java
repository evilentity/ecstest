
package io.piotrjastrzebski.edt.deferred.systems;

import com.artemis.BaseSystem;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

/**
 * Created by EvilEntity on 26/11/2015.
 */
public class CamBoundsSystem extends BaseSystem implements InputProcessor {
	private static final String TAG = CamBoundsSystem.class.getSimpleName();

//	@Wire ShapeRenderer renderer;
	@Wire OrthographicCamera camera;
	@Wire ExtendViewport viewport;

	public CamBoundsSystem () {}

	@Override protected void initialize () {
		InputMultiplexer multiplexer = (InputMultiplexer)Gdx.input.getInputProcessor();
		multiplexer.addProcessor(this);
	}

	@Override protected void processSystem () {
		updateCam();
		updateBounds(viewport, bounds);
//		renderer.setColor(Color.CYAN);
//		renderer.setProjectionMatrix(camera.combined);
//		renderer.begin(ShapeRenderer.ShapeType.Line);
//		renderer.rect(bounds.x, bounds.y, bounds.width, bounds.height);
//		renderer.setColor(Color.GOLD);
//		renderer.rect(extraBounds.x, extraBounds.y, extraBounds.width, extraBounds.height);
//		renderer.end();
	}

	private void updateCam () {
		float scale = (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT) || Gdx.input.isKeyPressed(Input.Keys.SHIFT_RIGHT))?10:1;
		if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
			camera.position.x -= scale * world.delta * 3;
		}
		if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
			camera.position.x += scale * world.delta * 3;
		}
		if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
			camera.position.y += scale * world.delta * 3;
		}
		if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
			camera.position.y -= scale * world.delta * 3;
		}
		camera.update();
	}

	private Rectangle bounds = new Rectangle();
	private Rectangle extraBounds = new Rectangle();
	private float margin = 5;
	private float vpScale = .75f;
	private float lastZoom = 0;
	private Rectangle updateBounds (ExtendViewport viewport, Rectangle bounds) {
		OrthographicCamera camera = (OrthographicCamera)viewport.getCamera();
		float x = camera.position.x;
		float y = camera.position.y;
		float width = viewport.getWorldWidth() * camera.zoom * vpScale;
		float height = viewport.getWorldHeight() * camera.zoom * vpScale;
		bounds.set(x - width/2, y - height/2, width, height);
		if (!extraBounds.contains(bounds) || lastZoom != camera.zoom) {
			lastZoom = camera.zoom;
			extraBounds.set(x - width / 2 - margin * camera.zoom, y - height / 2 - margin * camera.zoom, width + margin * 2 * camera.zoom, height + margin * 2 * camera.zoom);
		}
		return bounds;
	}

	public float getMargin () {
		return margin;
	}

	public void setMargin (float margin) {
		this.margin = margin;
	}

	public float getVpScale () {
		return vpScale;
	}

	public void setVpScale (float vpScale) {
		this.vpScale = vpScale;
	}

	@Override public boolean keyDown (int keycode) {
		return false;
	}

	@Override public boolean keyUp (int keycode) {
		return false;
	}

	@Override public boolean keyTyped (char character) {
		return false;
	}

	@Override public boolean touchDown (int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override public boolean touchUp (int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override public boolean touchDragged (int screenX, int screenY, int pointer) {
		return false;
	}

	@Override public boolean mouseMoved (int screenX, int screenY) {
		return false;
	}

	@Override public boolean scrolled (int amount) {
		camera.zoom = MathUtils.clamp(camera.zoom + camera.zoom * amount * 0.1f, 0.1f, 3f);
		return false;
	}
}
