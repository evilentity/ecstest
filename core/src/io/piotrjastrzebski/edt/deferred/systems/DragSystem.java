package io.piotrjastrzebski.edt.deferred.systems;

import box2dLight.Light;
import box2dLight.PointLight;
import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.EntityEdit;
import com.artemis.annotations.Wire;
import com.artemis.systems.IteratingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import io.piotrjastrzebski.edt.deferred.components.*;
import io.piotrjastrzebski.edt.deferred.components.assets.DiffuseAssetDef;
import io.piotrjastrzebski.edt.deferred.components.assets.NormalAssetDef;
import io.piotrjastrzebski.edt.utils.LightRenderer;

/**
 * Created by PiotrJ on 29/08/15.
 */
@Wire
public class DragSystem extends IteratingSystem {
	private ComponentMapper<Transform> mTransform;
	private ComponentMapper<Size> mSize;
	@Wire CursorSystem cs;
	@Wire LightRenderer lights;

	public DragSystem () {
		super(Aspect.all(Transform.class, Size.class, Drag.class));
	}
	Light light;
	@Override protected void initialize () {
		super.initialize();
		light = new PointLight(lights.rayHandler, 64, Color.WHITE, 7, 0, 0);
	}

	float cooldown;
	@Override protected void begin () {
		cooldown -= world.delta;
		if (cooldown <= 0 && Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
			cooldown = .5f;
			EntityEdit edit = world.edit(world.create());
			edit.create(DiffuseAssetDef.class).setPath("box64.png");
			edit.create(NormalAssetDef.class).setPath("n-box64.png");
			edit.create(Transform.class).setPosition(cs.x - 2, cs.y - 1);
			edit.create(Size.class).set(4, 2);
			edit.create(DebugColor.class).set(Color.RED);
			edit.create(Renderable.class);
			edit.create(StaticEntity.class);
			edit.create(AABB.class);
		}
		light.setPosition(cs.x, cs.y);
	}

	@Override protected void process (int entityId) {
		Transform transform = mTransform.get(entityId);
		Size size = mSize.get(entityId);
		if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
			transform.setPosition((int)(cs.x - size.width/2), (int)(cs.y - size.height/2));
		} else {
			transform.setPosition(cs.x - size.width/2, cs.y - size.height/2);
		}
		if (Gdx.input.isKeyPressed(Input.Keys.LEFT_BRACKET)) {
			transform.rotation += 90 * world.delta;
			if (transform.rotation > 360) transform.rotation -= 360;
		} else if (Gdx.input.isKeyPressed(Input.Keys.RIGHT_BRACKET)) {
			transform.rotation -= 90 * world.delta;
			if (transform.rotation < 0) transform.rotation += 360;
		}
	}
}
