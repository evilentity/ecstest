package io.piotrjastrzebski.edt.deferred.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.systems.IteratingSystem;
import io.piotrjastrzebski.edt.deferred.components.DismantleProgress;
import io.piotrjastrzebski.edt.deferred.components.assets.SpineAsset;

/**
 * Created by EvilEntity on 30/11/2015.
 */
public class SpineDismantleProgressSystem extends IteratingSystem {
	private ComponentMapper<DismantleProgress> mDismantleProgress;
	private ComponentMapper<SpineAsset> mSpineAsset;

	public SpineDismantleProgressSystem () {
		super(Aspect.all(DismantleProgress.class, SpineAsset.class));
	}

	@Override protected void initialize () {

	}

	@Override protected void inserted (int entityId) {
		SpineAsset asset = mSpineAsset.get(entityId);
//		AnimationState.TrackEntry current = spineAsset.state.getCurrent(0);
//		current.setLoop(false);
		// WTF why you no work!!!!
//		spineAsset.skeleton.update(1);
		asset.state.addAnimation(0, "deconstruct", false, 1);
//		spineAsset.state.setAnimation(0, "deconstruct", false);
	}

	@Override protected void process (int entityId) {
		SpineAsset asset = mSpineAsset.get(entityId);
		DismantleProgress progress = mDismantleProgress.get(entityId);
		float oldNorm = progress.value/progress.duration;
		progress.value += world.delta;
		float newNorm = progress.value/progress.duration;
//		asset.state.addAnimation(0, "deconstruct", false, 0);
//		asset.state.apply(asset.skeleton);
		asset.state.update(newNorm-oldNorm);
//		asset.state.update(world.delta);
		// last, we are done
		if (newNorm >= 1) {
//			Gdx.app.log("", "welp");
			world.delete(entityId);
			// NOTE or replace with new entity or whatever
		}
	}
}
