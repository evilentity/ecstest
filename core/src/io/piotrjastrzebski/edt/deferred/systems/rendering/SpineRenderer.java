package io.piotrjastrzebski.edt.deferred.systems.rendering;

import com.artemis.Aspect;
import com.artemis.BaseEntitySystem;
import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.esotericsoftware.spine.SkeletonRenderer;
import io.piotrjastrzebski.edt.deferred.components.Size;
import io.piotrjastrzebski.edt.deferred.components.Transform;
import io.piotrjastrzebski.edt.deferred.components.assets.DiffuseAsset;
import io.piotrjastrzebski.edt.deferred.components.assets.SpineAsset;

/**
 * Created by EvilEntity on 04/12/2015.
 */
public class SpineRenderer extends BaseEntitySystem {

	@Wire RendererProcessor processor;
	Renderer renderer;
	private ComponentMapper<SpineAsset> mSpineAsset;
	private ComponentMapper<Transform> mTransform;
	private ComponentMapper<Size> mSize;
	public SpineRenderer () {
		super(Aspect.all(RendererProcessor.Renderable.class, SpineAsset.class));
	}

	@Override protected void initialize () {
		processor.registerRenderer(renderer = new Renderer());
		// no need to process
		setEnabled(false);
	}

	@Override protected void inserted (int entityId) {
		processor.addRenderer(entityId, renderer);
	}

	private SkeletonRenderer skeletonRenderer = new SkeletonRenderer();
	private SpriteBatch batch;
	@Override protected void begin () {
		batch = processor.sprites;
	}

	public void process(int entityId) {
		SpineAsset asset = mSpineAsset.get(entityId);
//		Transform transform = mTransform.get(entityId);
//		Size size = mSize.get(entityId);
		// TODO update should be in other sytems, as is it is after normals, so normals are behind one frame
//		asset.state.update(world.delta);
//		asset.state.update(0);
//		asset.skeleton.setPosition(transform.x + size.width/2, transform.y + size.height/2);
//		asset.state.apply(asset.skeleton);
//		asset.skeleton.updateWorldTransform();
		skeletonRenderer.draw(batch, asset.skeleton);
	}

	@Override protected void end () {

	}

	@Override protected void removed (int entityId) {
		processor.removeRenderer(entityId, renderer);
	}

	@Override protected void processSystem () {}

	public class Renderer implements RendererProcessor.Renderer {
		@Override public int getUID () {
			// TODO how do we make this work? in insertion order?
			return 2;
		}

		@Override public Target getTarget () {
			return Target.DIFFUSE;
		}

		@Override public Type getType () {
			return Type.SPRITE;
		}

		@Override public void begin () {
			SpineRenderer.this.begin();
		}

		@Override public void process (int entityId) {
			SpineRenderer.this.process(entityId);
		}

		@Override public void end () {
			SpineRenderer.this.end();
		}
		@Override public boolean isActive () {
			return checkProcessing();
		}
	}

}
