package io.piotrjastrzebski.edt.deferred.systems.rendering;

import com.artemis.Aspect;
import com.artemis.BaseEntitySystem;
import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import io.piotrjastrzebski.edt.deferred.components.AABB;
import io.piotrjastrzebski.edt.deferred.components.Size;
import io.piotrjastrzebski.edt.deferred.components.Transform;
import io.piotrjastrzebski.edt.deferred.components.assets.DiffuseAsset;

/**
 * Created by EvilEntity on 04/12/2015.
 */
public class AABBRenderer extends BaseEntitySystem {

	@Wire RendererProcessor processor;
	Renderer renderer;
	private ComponentMapper<AABB> mAABB;
	public AABBRenderer () {
		super(Aspect.all(AABB.class));
	}

	@Override protected void initialize () {
		processor.registerRenderer(renderer = new Renderer());
		// no need to process
		setEnabled(false);
	}

	@Override protected void inserted (int entityId) {
		processor.addRenderer(entityId, renderer);
	}

	ShapeRenderer shapes;
	@Override protected void begin () {
		shapes = processor.shapes;
		shapes.setColor(Color.GOLD);
	}

	public void process(int entityId) {
		AABB aabb = mAABB.get(entityId);
		shapes.rect(aabb.x, aabb.y, aabb.width, aabb.height);
	}

	@Override protected void end () {

	}

	@Override protected void removed (int entityId) {
		processor.removeRenderer(entityId, renderer);
	}

	@Override protected void processSystem () {}

	public class Renderer implements RendererProcessor.Renderer {
		@Override public int getUID () {
			// TODO how do we make this work? in insertion order?
			return 9;
		}

		@Override public Target getTarget () {
			return Target.DIFFUSE_DEBUG;
		}

		@Override public Type getType () {
			return Type.SHAPE_LINE;
		}

		@Override public void begin () {
			AABBRenderer.this.begin();
		}

		@Override public void process (int entityId) {
			AABBRenderer.this.process(entityId);
		}

		@Override public void end () {
			AABBRenderer.this.end();
		}

		@Override public boolean isActive () {
			return checkProcessing();
		}
	}

}
