package io.piotrjastrzebski.edt.deferred.systems.rendering;

import com.artemis.Aspect;
import com.artemis.BaseEntitySystem;
import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.esotericsoftware.spine.SkeletonRenderer;
import com.esotericsoftware.spine.Skin;
import io.piotrjastrzebski.edt.deferred.components.Size;
import io.piotrjastrzebski.edt.deferred.components.Transform;
import io.piotrjastrzebski.edt.deferred.components.assets.SpineAsset;
import io.piotrjastrzebski.edt.utils.NormalBatch;

/**
 * Created by EvilEntity on 04/12/2015.
 */
public class NormalSpineRenderer extends BaseEntitySystem {

	@Wire RendererProcessor processor;
	Renderer renderer;
	private ComponentMapper<SpineAsset> mSpineAsset;
	private ComponentMapper<Transform> mTransform;
	private ComponentMapper<Size> mSize;
	public NormalSpineRenderer () {
		super(Aspect.all(RendererProcessor.Renderable.class, SpineAsset.class));
	}

	@Override protected void initialize () {
		processor.registerRenderer(renderer = new Renderer());
		// no need to process
		setEnabled(false);
	}

	@Override protected void inserted (int entityId) {
		processor.addRenderer(entityId, renderer);
	}

	private SkeletonRenderer skeletonRenderer = new SkeletonRenderer();
	private NormalBatch batch;
	@Override protected void begin () {
		batch = processor.normals;
	}

	public void process(int entityId) {
		// TODO need to change asset somehow to normals
		SpineAsset asset = mSpineAsset.getSafe(entityId);
		Transform transform = mTransform.get(entityId);
		Size size = mSize.get(entityId);
		// TODO this should be in separate system
		asset.state.update(world.delta);
		asset.skeleton.setPosition(transform.x + size.width/2, transform.y + size.height/2);
		asset.state.apply(asset.skeleton);
		asset.skeleton.updateWorldTransform();
		// switch skin to one with normals
		// todo make this more clever, skin ref in asset?
		Skin skin = asset.skeleton.getSkin();
		String name = skin.getName();
		asset.skeleton.setSkin(name+"-n");
		skeletonRenderer.draw(batch, asset.skeleton);
		// back to diffuse skin, or in diffuse renderer?
		asset.skeleton.setSkin(skin);
	}

	@Override protected void end () {

	}

	@Override protected void removed (int entityId) {
		processor.removeRenderer(entityId, renderer);
	}

	@Override protected void processSystem () {}

	public class Renderer implements RendererProcessor.Renderer {
		@Override public int getUID () {
			// TODO how do we make this work? in insertion order?
			return 6;
		}

		@Override public Target getTarget () {
			return Target.NORMALS;
		}

		@Override public Type getType () {
			return Type.NORMALS;
		}

		@Override public void begin () {
			NormalSpineRenderer.this.begin();
		}

		@Override public void process (int entityId) {
			NormalSpineRenderer.this.process(entityId);
		}

		@Override public void end () {
			NormalSpineRenderer.this.end();
		}
		@Override public boolean isActive () {
			return checkProcessing();
		}
	}

}
