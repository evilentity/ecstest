package io.piotrjastrzebski.edt.deferred.systems.rendering;

import com.artemis.*;
import com.artemis.annotations.Wire;
import com.artemis.utils.IntBag;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.LongArray;
import io.piotrjastrzebski.edt.deferred.components.Culled;
import io.piotrjastrzebski.edt.deferred.components.Transform;
import io.piotrjastrzebski.edt.deferred.systems.ViewBoundsSystem;
import io.piotrjastrzebski.edt.utils.NormalBatch;

import java.util.Arrays;

/**
 * We are using longs for metadata, on desktop speed difference between ints and longs is marginal
 * If needed, we can add more layers/renderers as a trade off in z ordering precision
 * It probably isn't on mobile, but we can compromise if needed, by using IntIntMap for meta -> id,
 * limiting z order precision and using part of the id in meta. There is an int version in git log.
 *
 * TODO remove asserts
 * Created by EvilEntity on 04/12/2015.
 */
public class RendererProcessor extends BaseSystem {
	private final static int RENDERER_NOOP = 0;
	// NOTE we have 64 bits in long, must skip last one for sign
	// so 63 to work with
//	public final static int TARGET_SHIFT = 27 + 32; // top 4 bits, -1 fo
	public final static int TARGET_MASK = 0xF; // 4 bits
//	public final static int RENDERER_SHIFT = 22 + 32;
	public final static int RENDERER_MASK = 0xF; // 4 bits, 16 values max
//	public final static int LAYER_SHIFT = 26 + 32;
	public final static int LAYER_MASK = 5; // 5 bits, 32 values max
	public final static int ENTITY_ID_MASK = 0xFFFFFF; // 24 bits, 16 mil, should be enough

	private ComponentMapper<Renderable> mRenderable;
	private ComponentMapper<RenderableChildren> mChildRenderable;
	private ComponentMapper<Transform> mTransform;

	private Renderer[] renderers;

	private LongArray longSorted = new LongArray();
	private EntitySubscription renderables;
	// TODO this is kinda janky
	@Wire SpriteBatch sprites;
	@Wire PolygonSpriteBatch polySprites;
	@Wire NormalBatch normals;
	@Wire ShapeRenderer shapes;
	@Wire OrthographicCamera camera;

	// NOTE getter only?
	FrameBuffer normalFBO;
	TextureRegion normalRegion;
	FrameBuffer specFBO;
	TextureRegion specRegion;

	@Wire ViewBoundsSystem vb;

	private boolean dirty = true;
	@Override protected void initialize () {
		renderables = world.getAspectSubscriptionManager().get(Aspect.all(Renderable.class).exclude(Culled.class));
		renderables.addSubscriptionListener(new EntitySubscription.SubscriptionListener() {
			@Override public void inserted (IntBag entities) {
				dirty = true;
			}

			@Override public void removed (IntBag entities) {
				dirty = true;
			}
		});
		renderers = new Renderer[16];
		// noop renderer as default
		// do we still need it?
		registerRenderer(new Renderer() {
			@Override public int getUID () {return 0;}
			@Override public Type getType () {return Type.SPRITE;}
			@Override public void begin () {}
			@Override public void process (int entityId) {}
			@Override public void end () {}
			@Override public boolean isActive () {return false;}
			@Override public Target getTarget () {return Target.NOOP;}
		});
		// TODO this needs to handle resize
		normalRegion = new TextureRegion();
		normalFBO = resizeFBO(normalFBO, normalRegion);

		specRegion = new TextureRegion(normalFBO.getColorBufferTexture());
		specFBO = resizeFBO(specFBO, specRegion);
		// TODO other fbos
	}

	private FrameBuffer resizeFBO(FrameBuffer fbo, TextureRegion region) {
		if (fbo != null) fbo.dispose();
		fbo = new FrameBuffer(Pixmap.Format.RGBA8888, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
		region.setRegion(fbo.getColorBufferTexture());
		region.flip(false, true);
		return fbo;
	}

	private int lastRenderer = RENDERER_NOOP;
//	private Renderer.Target lastTarget = Renderer.Target.NOOP;
	private Renderer.Type lastType;
	// TODO this probably doesn't need to be configurable
	// we can get away with a bit less precision for height, as it is smaller than width
	// since we are using longs, we can use a lot more precision for this
	private final static int Z_POT_X = 12;
	private final static int Z_POT_Y = 10;
//	private final static int Z_SHIFT = Z_POT_X + Z_POT_Y;
	private final static int Z_PRECISION_X = (int)Math.pow(2, Z_POT_X) - 1;
	private final static int Z_PRECISION_Y = (int)Math.pow(2, Z_POT_Y) - 1;
//	public final static int RENDERER_SHIFT = Z_SHIFT + Z_POT_X + Z_POT_Y;
	public final static int RENDERER_SHIFT = 24;
	private final static int Z_SHIFT = RENDERER_SHIFT + 4; // E_ID_MASK_LEN
	private final static int Z_MASK = 0x3FFFFF; // E_ID_MASK_LEN
//	public final static int LAYER_SHIFT = RENDERER_SHIFT + 4;
	public final static int LAYER_SHIFT = Z_SHIFT + Z_POT_X + Z_POT_Y;
	public final static int TARGET_SHIFT = LAYER_SHIFT + 5;
	// TODO does this need to be scaled with zoom?
	private final static float MARGIN = 2;


	private void print(String name, long value) {
		print(name, value, 64);
	}

	private void print(String name, long value, int zeroes) {
		System.out.print(name);
		System.out.print(": ");
		String bin = Long.toBinaryString(value);
		for (int k = 0; k < zeroes - bin.length(); k++) {
			System.out.print(" ");
		}
		System.out.print(bin);
		System.out.println(" (" + value + ")");
	}

	private void printMeta(String name, long meta) {
		System.out.println(name);
		int target = getTarget(meta);
		int layer = getLayer(meta);
		int z = getZ(meta);
		int zy = getZY(meta);
		int zx = getZX(meta);
		int renderer = getRenderer(meta);
		int eid = getEntityId(meta);
		print("  trgt ", target, 9);
		print("  layer", layer, 14);
		print("  z    ", z, 36);
		print("  zy   ", zy, 24);
		print("  zx   ", zx, 36);
		print("  rend ", renderer, 40);
		print("  eid  ", eid, 64);
		print("  raw  ", meta);
	}

	@Override protected void processSystem () {
		Gdx.app.log("", "begin FRAME");
		final Rectangle vb = this.vb.getViewBounds();

		if (dirty) {
			dirty = false;
			IntBag ids = renderables.getEntities();
			longSorted.size = 0;
			int[] rawIds = ids.getData();
			for (int i = 0, n = ids.size(); i < n; i++) {
				int id = rawIds[i];
				long z = 0;
				Transform transform = mTransform.getSafe(id);
				if (transform != null) {
					// NOTE we want to render stuff from top right corner to bottom left
					// so we flip z,y
					int x = Z_PRECISION_X - (int)(MathUtils.clamp((transform.x - vb.x + MARGIN) / (vb.width + MARGIN), 0, 1) * Z_PRECISION_X);
					int y = Z_PRECISION_Y - (int)(MathUtils.clamp((transform.y - vb.y + MARGIN) / (vb.height + MARGIN), 0, 1) * Z_PRECISION_Y);
					if (x > Z_PRECISION_X || x < 0)
						throw new AssertionError(" x > Z_PRECISION_X || x < 0");
					if (y > Z_PRECISION_Y || y < 0)
						throw new AssertionError(" y > Z_PRECISION_Y || y < 0");
					// pack y and x to z
					// y is first so entities lower on the screen overlap stuff higher on the screen
					z = (y << Z_POT_X | x);
					// shift to position
//				z = z << (LAYER_SHIFT - Z_SHIFT);
					z = z << Z_SHIFT;
				}
				Renderable renderable = mRenderable.get(id);
				// TODO check if layer is enabled?
//			long layer = (long)(31-renderable.layer) << LAYER_SHIFT;
				long layer = (long)(renderable.layer) << LAYER_SHIFT;
//				System.out.println("Entity " + id);
				int[] renderers = renderable.renderers;
				for (int j = 0; j < renderable.rendererCount; j++) {
					int uid = renderers[j];
					if (!isRendererActive(uid))
						continue;
					Renderer renderer = this.renderers[uid];
					Renderer.Target target = renderer.getTarget();
					long t = (long)target.id << TARGET_SHIFT;
//					print("target ", t);
//					print("layer  ", layer);
//					print("z      ", z);
//				print("uid   ", (long)uid << RENDERER_SHIFT);
//				long meta = layer | z | (long)uid << (RENDERER_SHIFT - Z_SHIFT) | id;
					// we really want z to be before renderer
					// but if it is first, stuff gets mixed due to need for normsls etc
					// another flag, diffuse/normals/whatever is in order
//				long meta = t | layer | (long)uid << (RENDERER_SHIFT) | z | id;
					long meta = t | layer | z | (long)uid << (RENDERER_SHIFT) | (id & ENTITY_ID_MASK);
					longSorted.add(meta);
//					print("meta  ", meta);
//					System.out.println(meta);
				}
			}

			Arrays.sort(longSorted.items, 0, longSorted.size);
//			for (int i = 0; i < longSorted.size; i++) {
//				long meta = longSorted.get(i);
//				printMeta("EID " + getEntityId(meta), meta);
//			}
		}

		for (int i = 0, n = longSorted.size; i < n; i++) {
			long meta = longSorted.items[i];
			int nextRenderer = getRenderer(meta);
			Renderer renderer = renderers[lastRenderer];
			if (nextRenderer != lastRenderer) {
				renderer.end();
				Renderer.Target last = renderer.getTarget();
				// this assumes that only valid renderer are stored in the metadata
				renderer = renderers[nextRenderer];
				Renderer.Target next = renderer.getTarget();
				Renderer.Type type = renderer.getType();
				if (type != lastType) {
					endRenderer(lastType);
					switchTarget(last, next);
//					if (last != next) endTarget(last);
					beginRenderer(type);
					lastType = type;
				}
				renderer.begin();
				lastRenderer = nextRenderer;
			}

			int eid = getEntityId(meta);
			if (eid == -1) throw new AssertionError("Invalid eid from meta!");
			renderer.process(eid);
			// NOTE children, if any are assumed to be using same renderer as parent
			// TODO make sure we don't render children multiple times
			RenderableChildren children = mChildRenderable.getSafe(eid);
			if (children != null) {
				int[] cIds = children.ids.items;
				for (int j = 0, m = children.ids.size; j < m; j++) {
					renderer.process(cIds[j]);
				}
			}
		}

		Renderer last = renderers[lastRenderer];
		last.end();
		endRenderer(lastType);
		endTarget(last.getTarget());

		lastRenderer = RENDERER_NOOP;
		lastType = null;
	}

	private void switchTarget (Renderer.Target last, Renderer.Target next) {
		if (last == next) return;
		endTarget(last);
		beginTarget(next);
	}

	private void beginTarget (Renderer.Target target) {
		Gdx.app.log("", "begin " + target);
		switch (target) {
		case NORMALS:
			// TODO clear in a system? or in first call in frame?
			normalFBO.begin();
			Gdx.gl.glClearColor(0.5f, 0.5f, 1f, 1f);
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
			break;
		case SPECULAR:
			break;
		case EMISSION:
			break;
		case DIFFUSE:
		case DIFFUSE_DEBUG:
			break;
		case LIGHTS:
			break;
		}
	}

	private void endTarget (Renderer.Target target) {
		Gdx.app.log("", "end " + target);
		switch (target) {
		case NORMALS:
			normalFBO.end();
			break;
		case SPECULAR:
			break;
		case EMISSION:
			break;
		}
	}

	private int getTarget (long meta) {
		int id = getRenderer(meta);
		return renderers[id].getTarget().id;
	}

	public int getLayer(long metadata) {
		return (int)(metadata >> LAYER_SHIFT & LAYER_MASK);
	}

	public int getRenderer(long metadata) {
//		return (int)(metadata >> (RENDERER_SHIFT - Z_SHIFT) & RENDERER_MASK);
		return (int)(metadata >> (RENDERER_SHIFT) & RENDERER_MASK);
	}

	public int getEntityId(long metadata) {
		// we just want the bottom 32 bits
		return (int)(metadata & ENTITY_ID_MASK);
	}

	private int getZ (long meta) {
		return (int)(meta >> Z_SHIFT & Z_MASK);
	}

	private int getZX (long meta) {
		int z = getZ(meta);
		return z & 0xFFF;
	}

	private int getZY (long meta) {
		int z = getZ(meta);
		return z >> 12 & 0x3FF;
	}

	private boolean isRendererActive (int uid) {
		final Renderer renderer = renderers[uid];
		return renderer != null && renderer.isActive();
	}

	private void beginRenderer (Renderer.Type type) {
		Gdx.app.log("", "  begin " + type);
		if (type == null) return;
		switch (type) {
		case LIGHTS:
			// no need to do anything
			break;
		case SPRITE:
			sprites.setProjectionMatrix(camera.combined);
			sprites.begin();
			break;
		case POLY_SPRITE:
			polySprites.setProjectionMatrix(camera.combined);
			polySprites.begin();
			break;
		case NORMALS:
			// TODO move this to target
//			normalFBO.begin();
			// TODO do we want this here
//			Gdx.gl.glClearColor(0.5f, 0.5f, 1f, 1f);
//			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
			normals.setProjectionMatrix(camera.combined);
			normals.begin();
			break;
//		case SPEC:
//			 TODO do we need special batch for spec?
//			specFBO.begin();
//			Gdx.gl.glClearColor(0, 0, 0, 1f);
//			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
//			sprites.setProjectionMatrix(camera.combined);
//			sprites.begin();
//			break;
		case SHAPE_LINE:
			shapes.setProjectionMatrix(camera.combined);
			shapes.begin(ShapeRenderer.ShapeType.Line);
			break;
		case SHAPE_FILLED:
			shapes.setProjectionMatrix(camera.combined);
			shapes.begin(ShapeRenderer.ShapeType.Filled);
			break;
		}
	}

	private void endRenderer (Renderer.Type type) {
		Gdx.app.log("", "  end " + type);
		if (type == null) return;
		switch (type) {
		case LIGHTS:
			// no need to do anything
			break;
		case SPRITE:
			sprites.end();
			break;
		case POLY_SPRITE:
			polySprites.end();
			break;
		case NORMALS:
			normals.end();
//			normalFBO.end();
			break;
//		case SPEC:
			// TODO do we need special batch for spec?
//			sprites.end();
//			specFBO.end();
//			break;
		case SHAPE_LINE:
		case SHAPE_FILLED:
			shapes.end();
			break;
		}
	}

	public void registerRenderer (Renderer renderer) {
		int uid = renderer.getUID();
		isRendererInBounds(uid);
		if (renderers[uid] != null)
			throw new AssertionError("Renderer UID is not unique! Conflict with " + renderers[uid]);
		renderers[uid] = renderer;
	}

	public void unregisterRenderer (Renderer renderer) {
		int uid = renderer.getUID();
		isRendererInBounds(uid);
		renderers[uid] = null;
	}

	// TODO do we want this in here?
	public Renderable addRenderer (int entityId, Renderer renderer) {
		Renderable renderable = mRenderable.create(entityId);
		int uid = renderer.getUID();
		isRendererInBounds(uid);
		renderable.renderers[renderable.rendererCount++] = renderer.getUID();
		return renderable;
	}

	// TODO do we want this in here?
	public void removeRenderer (int entityId, Renderer renderer) {
		Renderable renderable = mRenderable.getSafe(entityId);
		if (renderable == null) return;
		int uid = renderer.getUID();
		isRendererInBounds(uid);
		int[] renderers = renderable.renderers;
		int count = renderable.rendererCount;
		for (int i = 0; i < count; i++) {
			if (renderers[i] == uid) {
				// swap last with this one
				renderers[i] = 0;
				renderers[i] =	renderers[count-1];
				renderable.rendererCount--;
				return;
			}
		}
	}

	// TODO do we want this in here?
	public void setLayer (Renderable renderable, int layer) {
		if (layer < 0 || layer >= 32)
			throw new AssertionError("Layer outside of [0, 32] bounds!");
		renderable.layer = layer;
	}

	private void isRendererInBounds (int uid) {
		if (uid < 0 || uid >= renderers.length)
			throw new AssertionError("Renderer UID outside of [0, 15] bounds!");
	}

	public static class Renderable extends PooledComponent {
		public int metadata;
		// up to 8 renderers per entity
		public int renderers[] = new int[8];
		public int rendererCount = 0;
		public int layer;

		@Override protected void reset () {
			metadata = 0;
			rendererCount = 0;
			layer = 0;
		}

		public void setLayer (int layer) {
			this.layer = layer;
		}
	}

	public static class RenderableChildren extends PooledComponent {
		public final IntArray ids = new IntArray();

		@Override protected void reset () {
			ids.clear();
		}
	}

	public interface Renderer {
		enum Target {
			// we dont need to get these from meta, we just need the id to sort
			NOOP(0),
//			NORMALS(1), SPECULAR(1 << 1), EMISSION(1 << 2),
			NORMALS(1), SPECULAR(2), EMISSION(3),
//			DIFFUSE(1 << 3), DIFFUSE_DEBUG(1 << 4),
			DIFFUSE(4), DIFFUSE_DEBUG(5),
//			LIGHTS(1 << 5);
			LIGHTS(6);
			public final int id;
			Target (int mask) {
				this.id = mask;
			}
		}
		enum Type {SPRITE, POLY_SPRITE, NORMALS, SPEC, SHAPE_LINE, SHAPE_FILLED, LIGHTS}
		// NOTE this is kinda dumb, but we need persistent ids
		int getUID ();
		Type getType ();
		Target getTarget ();
		void begin ();
		void process (int entityId);
		void end ();
		boolean isActive ();
	}

	@Override protected void dispose () {
		super.dispose();
		normalFBO.dispose();
		specFBO.dispose();
	}
}
