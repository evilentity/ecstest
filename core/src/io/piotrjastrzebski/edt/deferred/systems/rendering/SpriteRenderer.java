package io.piotrjastrzebski.edt.deferred.systems.rendering;

import com.artemis.Aspect;
import com.artemis.BaseEntitySystem;
import com.artemis.BaseSystem;
import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import io.piotrjastrzebski.edt.deferred.components.Size;
import io.piotrjastrzebski.edt.deferred.components.Transform;
import io.piotrjastrzebski.edt.deferred.components.assets.Asset;
import io.piotrjastrzebski.edt.deferred.components.assets.DiffuseAsset;

/**
 * Created by EvilEntity on 04/12/2015.
 */
public class SpriteRenderer extends BaseEntitySystem {

	@Wire RendererProcessor processor;
	Renderer renderer;
	private ComponentMapper<DiffuseAsset> mDiffuseAsset;
	private ComponentMapper<Transform> mTransform;
	private ComponentMapper<Size> mSize;
	public SpriteRenderer () {
		super(Aspect.all(RendererProcessor.Renderable.class, DiffuseAsset.class));
	}

	@Override protected void initialize () {
		processor.registerRenderer(renderer = new Renderer());
		// no need to process
		setEnabled(false);
	}

	@Override protected void inserted (int entityId) {
		processor.addRenderer(entityId, renderer);
	}

	SpriteBatch batch;
	@Override protected void begin () {
		batch = processor.sprites;
	}

	public void process(int entityId) {
		DiffuseAsset asset = mDiffuseAsset.get(entityId);
		Transform transform = mTransform.get(entityId);
		Size size = mSize.get(entityId);
		batch.draw(asset.region,
			transform.x + asset.xOffset,
			transform.y + asset.yOffset,
			size.width/2 - asset.xOffset, size.height/2 - asset.yOffset,
			asset.width, asset.height,
			1, 1, transform.rotation);
	}

	@Override protected void end () {

	}

	@Override protected void removed (int entityId) {
		processor.removeRenderer(entityId, renderer);
	}

	@Override protected void processSystem () {}

	public class Renderer implements RendererProcessor.Renderer {
		@Override public int getUID () {
			// TODO how do we make this work? in insertion order?
			return 1;
		}

		@Override public Target getTarget () {
			return Target.DIFFUSE;
		}

		@Override public Type getType () {
			return Type.SPRITE;
		}

		@Override public void begin () {
			SpriteRenderer.this.begin();
		}

		@Override public void process (int entityId) {
			SpriteRenderer.this.process(entityId);
		}

		@Override public void end () {
			SpriteRenderer.this.end();
		}
		@Override public boolean isActive () {
			return checkProcessing();
		}
	}

}
