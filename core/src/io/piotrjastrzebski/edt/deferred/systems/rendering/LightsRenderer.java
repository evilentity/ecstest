package io.piotrjastrzebski.edt.deferred.systems.rendering;

import com.artemis.*;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.graphics.Texture;
import io.piotrjastrzebski.edt.deferred.components.LightsRendering;
import io.piotrjastrzebski.edt.deferred.components.Size;
import io.piotrjastrzebski.edt.deferred.components.Transform;
import io.piotrjastrzebski.edt.deferred.components.assets.DiffuseAsset;

/**
 * Created by EvilEntity on 04/12/2015.
 */
public class LightsRenderer extends BaseSystem {

	@Wire RendererProcessor processor;
	Renderer renderer;
	private ComponentMapper<DiffuseAsset> mDiffuseAsset;
	private ComponentMapper<Transform> mTransform;
	private ComponentMapper<Size> mSize;

	@Wire io.piotrjastrzebski.edt.utils.LightRenderer lightRenderer;

	public LightsRenderer () {}

	@Override protected void initialize () {
		processor.registerRenderer(renderer = new Renderer());
		// no need to process
		setEnabled(false);
		// create dummy renderable that will trigger rendering
		EntityEdit edit = world.createEntity().edit();
		processor.addRenderer(edit.getEntityId(), renderer).setLayer(10);
		edit.create(LightsRendering.class);
	}

	@Override protected void begin () {
		Texture normals = processor.normalFBO.getColorBufferTexture();
		// TODO render lights with normals etc
		lightRenderer.render(normals);
	}

	@Override protected void processSystem () {}

	public class Renderer implements RendererProcessor.Renderer {
		@Override public int getUID () {
			// TODO how do we make this work? in insertion order?
			// NOTE this should go after all fbos needed by lights are renderer
			return 7;
		}

		@Override public Type getType () {
			return Type.LIGHTS;
		}

		@Override public Target getTarget () {
			return Target.LIGHTS;
		}

		@Override public void begin () {
			LightsRenderer.this.begin();
		}

		@Override public void process (int entityId) {}

		@Override public void end () {
			LightsRenderer.this.end();
		}
		@Override public boolean isActive () {
			return checkProcessing();
		}
	}

}
