package io.piotrjastrzebski.edt.deferred.systems.rendering;

import com.artemis.Aspect;
import com.artemis.BaseEntitySystem;
import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import io.piotrjastrzebski.edt.deferred.components.Size;
import io.piotrjastrzebski.edt.deferred.components.Transform;
import io.piotrjastrzebski.edt.deferred.components.assets.NormalAsset;
import io.piotrjastrzebski.edt.utils.NormalBatch;

/**
 * Created by EvilEntity on 04/12/2015.
 */
public class NormalSpriteRenderer extends BaseEntitySystem {

	@Wire RendererProcessor processor;
	Renderer renderer;
	private ComponentMapper<NormalAsset> mNormalAsset;
	private ComponentMapper<Transform> mTransform;
	private ComponentMapper<Size> mSize;
	public NormalSpriteRenderer () {
		super(Aspect.all(RendererProcessor.Renderable.class, NormalAsset.class));
	}

	@Override protected void initialize () {
		processor.registerRenderer(renderer = new Renderer());
		// no need to process
		setEnabled(false);
	}

	@Override protected void inserted (int entityId) {
		processor.addRenderer(entityId, renderer);
	}

	private NormalBatch normals;
	@Override protected void begin () {
		normals = processor.normals;
		// TODO need to start fbo for stuff
	}

	public void process(int entityId) {
		NormalAsset asset = mNormalAsset.get(entityId);
		Transform transform = mTransform.get(entityId);
		Size size = mSize.get(entityId);
		normals.draw(asset.region,
			transform.x + asset.xOffset,
			transform.y + asset.yOffset,
			size.width/2 - asset.xOffset, size.height/2 - asset.yOffset,
			asset.width, asset.height,
			1, 1, transform.rotation);
	}

	@Override protected void end () {

	}

	@Override protected void removed (int entityId) {
		processor.removeRenderer(entityId, renderer);
	}

	@Override protected void processSystem () {}

	public class Renderer implements RendererProcessor.Renderer {
		@Override public int getUID () {
			// TODO how do we make this work? in insertion order?
			return 5;
		}

		@Override public Target getTarget () {
			return Target.NORMALS;
		}

		@Override public Type getType () {
			return Type.NORMALS;
		}

		@Override public void begin () {
			NormalSpriteRenderer.this.begin();
		}

		@Override public void process (int entityId) {
			NormalSpriteRenderer.this.process(entityId);
		}

		@Override public void end () {
			NormalSpriteRenderer.this.end();
		}
		@Override public boolean isActive () {
			return checkProcessing();
		}
	}

}
