package io.piotrjastrzebski.edt.deferred.systems.rendering;

import com.artemis.BaseSystem;
import com.artemis.EntityEdit;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.math.Rectangle;
import io.piotrjastrzebski.edt.deferred.components.DebugNormalRendering;
import io.piotrjastrzebski.edt.deferred.systems.ViewBoundsSystem;

/**
 * Created by EvilEntity on 04/12/2015.
 */
public class DebugNormalRenderer extends BaseSystem {

	@Wire RendererProcessor processor;
	Renderer renderer;

	@Override protected void initialize () {
		processor.registerRenderer(renderer = new Renderer());
		// no need to process
		setEnabled(false);
		// create dummy renderable that will trigger rendering
		EntityEdit edit = world.createEntity().edit();
		processor.addRenderer(edit.getEntityId(), renderer).setLayer(11);
		edit.create(DebugNormalRendering.class);
	}

	@Wire ViewBoundsSystem vbs;
	@Override protected void begin () {
		final Rectangle vb = vbs.getViewBounds();
		processor.sprites.draw(processor.normalRegion, vb.x, vb.y, vb.width, vb.height);
	}

	@Override protected void processSystem () {}

	public class Renderer implements RendererProcessor.Renderer {
		@Override public int getUID () {
			// TODO how do we make this work? in insertion order?
			return 8;
		}

		@Override public Target getTarget () {
			return Target.DIFFUSE_DEBUG;
		}

		@Override public Type getType () {
			return Type.SPRITE;
		}

		@Override public void begin () {
			DebugNormalRenderer.this.begin();
		}

		@Override public void process (int entityId) {}

		@Override public void end () {
			DebugNormalRenderer.this.end();
		}
		@Override public boolean isActive () {
			return checkProcessing();
		}
	}

}
