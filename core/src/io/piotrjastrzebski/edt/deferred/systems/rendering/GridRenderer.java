package io.piotrjastrzebski.edt.deferred.systems.rendering;

import com.artemis.Aspect;
import com.artemis.BaseEntitySystem;
import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import io.piotrjastrzebski.edt.deferred.components.DebugGrid;
import io.piotrjastrzebski.edt.deferred.components.Size;
import io.piotrjastrzebski.edt.deferred.components.Transform;
import io.piotrjastrzebski.edt.deferred.components.assets.DiffuseAsset;
import io.piotrjastrzebski.edt.deferred.systems.ViewBoundsSystem;

/**
 * Created by EvilEntity on 04/12/2015.
 */
public class GridRenderer extends BaseEntitySystem {

	@Wire RendererProcessor processor;
	Renderer renderer;
	public GridRenderer () {
		super(Aspect.all(RendererProcessor.Renderable.class, DebugGrid.class));
	}

	ShapeRenderer shapes;
	ViewBoundsSystem vb;
	@Override protected void initialize () {
		processor.registerRenderer(renderer = new Renderer());
		shapes = processor.shapes;
		// no need to process
		setEnabled(false);
	}

	@Override protected void inserted (int entityId) {
		processor.addRenderer(entityId, renderer);
	}

	@Override protected void begin () {
//		Gdx.gl.glEnable(GL20.GL_BLEND);
//		Gdx.app.log("", "Grid");
		shapes.setColor(.1f, .1f, .1f, 0.1f);
		Rectangle bounds = vb.getViewBounds();
		float width = bounds.width + 4;
		float height = bounds.height + 4;
		float x = (int)(bounds.x - 2);
		x -= x%2;
		float y = (int)(bounds.y - 2);
		y -= y%2;
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				if (i % 2 == j % 2) continue;
				shapes.rect(x + i, y + j, 1, 1);
			}
		}
	}

	public void process(int entityId) {

	}

	@Override protected void end () {

	}

	@Override protected void removed (int entityId) {
		processor.removeRenderer(entityId, renderer);
	}

	@Override protected void processSystem () {}

	public class Renderer implements RendererProcessor.Renderer {
		@Override public int getUID () {
			// TODO how do we make this work? in insertion order?
			// NOTE this should go after all fbos needed by lights are renderer
			return 11;
		}

		@Override public Target getTarget () {
			return Target.DIFFUSE;
		}

		@Override public Type getType () {
			return Type.SHAPE_FILLED;
		}

		@Override public void begin () {
			GridRenderer.this.begin();
		}

		@Override public void process (int entityId) {
			GridRenderer.this.process(entityId);
		}

		@Override public void end () {
			GridRenderer.this.end();
		}
		@Override public boolean isActive () {
			return checkProcessing();
		}
	}

}
