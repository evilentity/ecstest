package io.piotrjastrzebski.edt.deferred.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.systems.IteratingSystem;
import com.badlogic.gdx.Gdx;
import io.piotrjastrzebski.edt.deferred.components.BuildProgress;
import io.piotrjastrzebski.edt.deferred.components.assets.SpineAsset;

/**
 * Created by EvilEntity on 30/11/2015.
 */
public class SpineBuildProgressSystem extends IteratingSystem {
	private ComponentMapper<BuildProgress> mBuildProgress;
	private ComponentMapper<SpineAsset> mSpineAsset;

	public SpineBuildProgressSystem () {
		super(Aspect.all(BuildProgress.class, SpineAsset.class));
	}

	@Override protected void initialize () {

	}

	@Override protected void process (int entityId) {
		BuildProgress progress = mBuildProgress.get(entityId);
		float oldNorm = progress.value/progress.duration;
		progress.value += world.delta;
		float newNorm = progress.value/progress.duration;
		SpineAsset asset = mSpineAsset.get(entityId);
		// how to we update this properly?
//		asset.state.update(newNorm - oldNorm);
		asset.state.update(world.delta);
		// last, we are done
		if (newNorm>=1) {
//			progress.value = 0;
//			asset.state.addAnimation(0, "construct", false, 0);
//			asset.state.addAnimation(0, "idle", false, 0);
//			asset.state.addAnimation(0, "deconstruct", false, 0);
			mBuildProgress.remove(entityId);
			Gdx.app.log("", "removed " + entityId);
		}
//		if (variant.current == variant.count - 1) {
//			mBuildProgress.remove(entityId);
			// NOTE or replace with new entity or whatever
//		}
	}
}
