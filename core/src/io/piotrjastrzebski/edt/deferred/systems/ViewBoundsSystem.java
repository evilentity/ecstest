package io.piotrjastrzebski.edt.deferred.systems;

import com.artemis.BaseSystem;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

/**
 * Created by EvilEntity on 03/12/2015.
 */
public class ViewBoundsSystem extends BaseSystem {
	@Wire OrthographicCamera camera;
	@Wire ExtendViewport viewport;

	private float margin = 5;
//	private float vpScale = .75f;
	private float vpScale = 1f;
	private float lastZoom = -1;
	private Rectangle viewBounds = new Rectangle();
	private Rectangle extraBounds = new Rectangle();
	private boolean extraDirty = true;
	@Override protected void processSystem () {
		extraDirty = false;
		float x = camera.position.x;
		float y = camera.position.y;
		float width = viewport.getWorldWidth() * camera.zoom * vpScale;
		float height = viewport.getWorldHeight() * camera.zoom * vpScale;
		viewBounds.set(x - width/2, y - height/2, width, height);
		if (!extraBounds.contains(viewBounds) || camera.zoom != lastZoom) {
			extraDirty = true;
			lastZoom = camera.zoom;
			float margin = this.margin * camera.zoom;
			extraBounds.set(x - width / 2 - margin, y - height / 2 - margin, width + margin * 2, height + margin * 2);
		}
	}

	public Rectangle getViewBounds () {
		return viewBounds;
	}

	public Rectangle getExtraBounds () {
		return extraBounds;
	}

	public boolean isExtraBoundsDirty () {
		return extraDirty;
	}
}
