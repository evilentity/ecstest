package io.piotrjastrzebski.edt.deferred.systems;

import com.artemis.BaseSystem;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by PiotrJ on 29/08/15.
 */
@Wire
public class CursorSystem extends BaseSystem {
	@Wire OrthographicCamera camera;

	public Vector2 cursor = new Vector2();
	public float x, y;
	private Vector3 tmp = new Vector3();

	@Override protected void processSystem () {
		camera.unproject(tmp.set(Gdx.input.getX(), Gdx.input.getY(), 0));
		cursor.set(x = tmp.x, y = tmp.y);
	}
}
