

package io.piotrjastrzebski.edt.deferred.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.artemis.systems.IteratingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import io.piotrjastrzebski.edt.deferred.components.*;
import io.piotrjastrzebski.edt.deferred.components.assets.SpineAsset;

/**
 * Created by EvilEntity on 26/11/2015.
 */
public class AABBSystem extends IteratingSystem {
	private static final String TAG = AABBSystem.class.getSimpleName();
	private ComponentMapper<Transform> mTransform;
	private ComponentMapper<Size> mSize;
	private ComponentMapper<AABB> mAABB;
	private ComponentMapper<SpineAsset> mSpineAsset;

	// TODO static things could be updated only on insert
	public AABBSystem () {
		super(Aspect.all(Transform.class, Size.class, AABB.class));
	}

	private Vector2 tmpOffset = new Vector2();
	private Vector2 tmpSize = new Vector2();
	@Override protected void process (int entityId) {
		Transform transform = mTransform.get(entityId);
		Size size = mSize.get(entityId);
		AABB aabb = mAABB.get(entityId);
		// TODO can we do this without this dumb check? probably not
		SpineAsset asset = mSpineAsset.getSafe(entityId);
		if (asset != null) {
			// NOTE more expensive than simple region, got a bunch of them in there
			asset.skeleton.getBounds(tmpOffset, tmpSize);
			aabb.set(tmpOffset.x, tmpOffset.y, tmpSize.x, tmpSize.y);
		} else {
			updateAABB(aabb, transform, size);
		}
	}

	private void updateAABB (AABB aabb, Transform transform, Size size) {
		// we want distance, so always positive values
		float sin = MathUtils.sinDeg(transform.rotation);
		if (sin < 0) sin = -sin;
		float cos = MathUtils.cosDeg(transform.rotation);
		if (cos < 0) cos = -cos;
		float width = size.width * cos + size.height * sin;
		float height = size.width * sin + size.height * cos;
		float x = transform.x + (size.width - width)/2;
		float y = transform.y + (size.height - height)/2;
		aabb.set(x, y, width, height);
	}
}
