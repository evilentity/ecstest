package io.piotrjastrzebski.edt.deferred.systems.assets;

import com.artemis.Aspect;
import com.artemis.BaseEntitySystem;
import com.artemis.ComponentMapper;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.TextureData;
import com.badlogic.gdx.graphics.g2d.PixmapPacker;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.StringBuilder;
import com.esotericsoftware.spine.*;
import com.esotericsoftware.spine.attachments.*;
import io.piotrjastrzebski.edt.TestScreen;
import io.piotrjastrzebski.edt.deferred.components.ViewDirection;
import io.piotrjastrzebski.edt.deferred.components.Size;
import io.piotrjastrzebski.edt.deferred.components.Variant;
import io.piotrjastrzebski.edt.deferred.components.assets.*;

import java.util.Comparator;

/**
 * Created by EvilEntity on 26/11/2015.
 */
public class AssetsManager extends BaseEntitySystem {
	// TODO we dont want this crap here
	private ComponentMapper<DiffuseAssetDef> mDiffuseAssetDef;
	private ComponentMapper<DiffuseAsset> mDiffuseAsset;
	private ComponentMapper<NormalAssetDef> mNormalAssetDef;
	private ComponentMapper<NormalAsset> mNormalAsset;
	private ComponentMapper<SpineAssetDef> mSpineAssetDef;
	private ComponentMapper<SpineAsset> mSpineAsset;
	private ComponentMapper<ViewDirection> mFacing;
	private ComponentMapper<Variant> mVariant;
	private ComponentMapper<Size> mSize;

	private Array<PathModifier> modifiers = new Array<PathModifier>();
	private PathModifier extModifier;

	public AssetsManager () {
		super(Aspect.one(DiffuseAssetDef.class, NormalAssetDef.class, SpineAssetDef.class));
	}

	TextureAtlas spineAtlas;
	TextureAtlas spineNormalsAtlas;
	@Override protected void initialize () {
		addModifier(extModifier = new PathModifier() {
			@Override public StringBuilder execute (StringBuilder path, int entityId) {
				// TODO kinda hacky
				if (path.indexOf(".spine") != -1) return path;
				return path.indexOf(".png") != -1?path:path.append(".png");
			}
			@Override public int order () {return 100;}

			@Override public String toString () {
				return "Extension{order=100}";
			}
		});

		// lets try prefix for normals etc, suffix for other things
		// cba to pack, make fake atlases
		spineAtlas = createAtlas(
				"cube/square.png",
				"cube/square2.png",
				"building/build-0-e.png",
				"building/build-0-n.png",
				"building/build-0-s.png",
				"building/build-0-w.png",
				"building/build-1-e.png",
				"building/build-1-n.png",
				"building/build-1-s.png",
				"building/build-1-w.png",
				"building/build-2-e.png",
				"building/build-2-n.png",
				"building/build-2-s.png",
				"building/build-2-w.png",
				"building/build-3-e.png",
				"building/build-3-n.png",
				"building/build-3-s.png",
				"building/build-3-w.png",
				"box64.png",
				"badlogic.jpg",
				"nut.png"
		);
		spineNormalsAtlas = createAtlas(
				"cube/n-square.png",
				"cube/n-square2.png",
				"building/build-0-e.png",
				"building/build-0-n.png",
				"building/build-0-s.png",
				"building/build-0-w.png",
				"building/build-1-e.png",
				"building/build-1-n.png",
				"building/build-1-s.png",
				"building/build-1-w.png",
				"building/build-2-e.png",
				"building/build-2-n.png",
				"building/build-2-s.png",
				"building/build-2-w.png",
				"building/build-3-e.png",
				"building/build-3-n.png",
				"building/build-3-s.png",
				"building/build-3-w.png",
				"n-box64.png",
				"n-nut.png"
		);

		skeletonJson = new SkeletonJson(new AttachmentLoader() {
			@Override public RegionAttachment newRegionAttachment (Skin skin, String name, String path) {
				RegionAttachment attachment = new RegionAttachment(name);
				attachment.setRegion(getRegion(name));
				return attachment;
			}

			@Override public MeshAttachment newMeshAttachment (Skin skin, String name, String path) {
				MeshAttachment attachment = new MeshAttachment(name);
				attachment.setRegion(getRegion(name));
				return attachment;
			}

			@Override public SkinnedMeshAttachment newSkinnedMeshAttachment (Skin skin, String name, String path) {
				SkinnedMeshAttachment attachment = new SkinnedMeshAttachment(name);
				attachment.setRegion(getRegion(name));
				return attachment;
			}

			@Override public BoundingBoxAttachment newBoundingBoxAttachment (Skin skin, String name) {
				return new BoundingBoxAttachment(name);
			}
		});
		skeletonJson.setScale(TestScreen.INV_SCALE);
	}

	private TextureAtlas createAtlas(String... paths) {
		PixmapPacker packer = new PixmapPacker(2048, 2048, Pixmap.Format.RGBA8888, 2, true);
		for (String path : paths) {
			Texture texture = new Texture(path);
			TextureData data = texture.getTextureData();
			data.prepare();
			// NOTE by doing this, we might end up with conflicts, probably a bad idea in real thing
			int lastSlash = path.lastIndexOf('/');
			if (lastSlash >= 0) {
				path = path.substring(lastSlash + 1);
			}
			int lastPng = path.lastIndexOf(".png");
			if (lastPng >= 0) {
				path = path.substring(0, lastPng);
			}
			packer.pack(path, data.consumePixmap());
			texture.dispose();
		}
		TextureAtlas atlas = packer.generateTextureAtlas(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear, false);
		packer.dispose();
		return atlas;
	}

	private Comparator<PathModifier> sorter = new Comparator<PathModifier>() {
		@Override public int compare (PathModifier o1, PathModifier o2) {
			return (o1.order() - o2.order());
		}
	};

	private ObjectMap<String, TextureRegion> diffuseRegions = new ObjectMap<String, TextureRegion>();
	private ObjectMap<String, TextureRegion> normalRegions = new ObjectMap<String, TextureRegion>();
	@Override protected void inserted (int entityId) {
		load(entityId);
	}

	public void load(int entityId) {
		unload(entityId);
		load(entityId, mDiffuseAssetDef, mDiffuseAsset);
		load(entityId, mNormalAssetDef, mNormalAsset);
		loadSpine(entityId, mSpineAsset, mSpineAssetDef);
	}

	private SkeletonJson skeletonJson;
	private void loadSpine (int entityId, ComponentMapper<SpineAsset> mSpineAsset, ComponentMapper<SpineAssetDef> mSpineAssetDef) {
		SpineAssetDef assetDef = mSpineAssetDef.getSafe(entityId);
		if (assetDef == null) return;
		SpineAsset asset = mSpineAsset.create(entityId);
		SkeletonData skeletonData = skeletonJson.readSkeletonData(Gdx.files.internal(assetDef.path));
		asset.skeleton = new Skeleton(skeletonData);
		if (assetDef.skin != null) asset.skeleton.setSkin(assetDef.skin);
		AnimationStateData stateData = new AnimationStateData(skeletonData);
		asset.state = new AnimationState(stateData);
		if (assetDef.anim != null)
			asset.state.setAnimation(0, assetDef.anim, assetDef.loop);
		// slow down so we can observe!
		asset.state.setTimeScale(assetDef.animScale);
	}

	private StringBuilder sb = new StringBuilder();
	private void load(int entityId, ComponentMapper<? extends AssetDef> getter, ComponentMapper<? extends Asset> setter) {
		AssetDef def = getter.getSafe(entityId);
		if (def == null) return;
		sb.setLength(0);
		sb.append(def.path);
		for (PathModifier modifier : modifiers) {
			modifier.execute(sb, entityId);
		}
		Asset asset = setter.create(entityId);
		asset.setRegion(getRegion(sb.toString()));
		Size size = mSize.get(entityId);
		asset.setOffsets(def.xOffset, def.yOffset);
		asset.width = def.width > 0?def.width:size.width;
		asset.height = def.height > 0?def.height:size.height;
	}

	private TextureRegion getRegion (String path) {
		String name = path;
		int lastSlash = name.lastIndexOf('/');
		if (lastSlash >= 0) {
			int lastPng = name.lastIndexOf(".png");
			if (lastPng >= 0) {
				name = name.substring(lastSlash + 1, lastPng);
			} else {
				name = name.substring(lastSlash + 1);
			}
		} else {
			int lastPng = name.lastIndexOf(".png");
			if (lastPng >= 0) {
				name = name.substring(0, lastPng);
			}
		}
		ObjectMap<String, TextureRegion> cache = name.startsWith("n-")?normalRegions:diffuseRegions;
		TextureRegion region = cache.get(path, null);
		if (region == null) {
			if (name.startsWith("n-")) {
				cache.put(path, region = spineNormalsAtlas.findRegion(name));
			} else  {
				cache.put(path, region = spineAtlas.findRegion(name));
			}
		}
		return region;
	}

	@Override protected void removed (int entityId) {
		// NOTE in real game, cleanup if needed
		unload(entityId);
	}

	public void unload(int entityId) {
		// do stuff
	}

	@Override protected void processSystem () {}

	@Override protected void dispose () {
		spineAtlas.dispose();
		spineNormalsAtlas.dispose();
	}

	public void addModifier (PathModifier modifier) {
		modifiers.add(modifier);
		modifiers.sort(sorter);
	}

	private Array<PathModifier> copy = new Array<PathModifier>();
	public Array<PathModifier> getModifiers () {
		copy.clear();
		copy.addAll(modifiers);
		return copy;
	}

	public interface PathModifier {
		StringBuilder execute (StringBuilder path, int entityId);
		int order();
	}
}
