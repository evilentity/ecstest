package io.piotrjastrzebski.edt.deferred.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.artemis.systems.IteratingSystem;
import com.badlogic.gdx.math.MathUtils;
import io.piotrjastrzebski.edt.deferred.components.DismantleProgress;
import io.piotrjastrzebski.edt.deferred.components.ViewDirection;
import io.piotrjastrzebski.edt.deferred.components.Variant;
import io.piotrjastrzebski.edt.deferred.systems.assets.AssetsManager;

/**
 * Created by EvilEntity on 30/11/2015.
 */
public class DismantleProgressSystem extends IteratingSystem {
	private ComponentMapper<DismantleProgress> mDismantleProgress;
	private ComponentMapper<Variant> mVariant;
	private ComponentMapper<ViewDirection> mFacing;

	@Wire AssetsManager assets;

	public DismantleProgressSystem () {
		super(Aspect.all(DismantleProgress.class, Variant.class));
	}

	@Override protected void initialize () {

	}

	@Override protected void process (int entityId) {
		DismantleProgress progress = mDismantleProgress.get(entityId);
		progress.value += world.delta;
		Variant variant = mVariant.get(entityId);
		float normalized = progress.value/progress.duration;
		int id = variant.count - (int)(normalized * (variant.count)) - 1;

		id = MathUtils.clamp(id, 0, variant.count -1);
		// update stuff if progress changed
		if (variant.current != id) {
			variant.current = id;
			assets.load(entityId);
		}
		// last, we are done
		if (variant.current == 0 && normalized >= 1) {
			world.delete(entityId);
			// NOTE or replace with new entity or whatever
		}
	}
}
