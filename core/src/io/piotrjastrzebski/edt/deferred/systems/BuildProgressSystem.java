package io.piotrjastrzebski.edt.deferred.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.artemis.systems.IteratingSystem;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.StringBuilder;
import io.piotrjastrzebski.edt.deferred.components.BuildProgress;
import io.piotrjastrzebski.edt.deferred.components.ViewDirection;
import io.piotrjastrzebski.edt.deferred.components.Variant;
import io.piotrjastrzebski.edt.deferred.systems.assets.AssetsManager;

/**
 * Created by EvilEntity on 30/11/2015.
 */
public class BuildProgressSystem extends IteratingSystem {
	private ComponentMapper<BuildProgress> mBuildProgress;
	private ComponentMapper<Variant> mVariant;
	private ComponentMapper<ViewDirection> mFacing;

	@Wire AssetsManager assets;

	public BuildProgressSystem () {
		super(Aspect.all(BuildProgress.class, Variant.class));
	}

	@Override protected void initialize () {
		assets.addModifier(new AssetsManager.PathModifier() {
			@Override public StringBuilder execute (StringBuilder path, int entityId) {
				Variant variant = mVariant.getSafe(entityId);
				if (variant != null) {
					path.append("-").append(variant.current);
				}
				return path;
			}
			@Override public int order () {
				return 0;
			}
			@Override public String toString () {
				return "Variant{order=0}";
			}
		});
		// where should this be? some sort of facing system...
		assets.addModifier(new AssetsManager.PathModifier() {
			@Override public StringBuilder execute (StringBuilder path, int entityId) {
				ViewDirection viewDirection = mFacing.getSafe(entityId);
				if (viewDirection != null) {
					path.append(viewDirection.side.getSuffix());
				}
				return path;
			}
			@Override public int order () {
				return 1;
			}
			@Override public String toString () {
				return "Facing{order=1}";
			}
		});
	}

	@Override protected void process (int entityId) {
		BuildProgress progress = mBuildProgress.get(entityId);
		progress.value += world.delta;
		Variant variant = mVariant.get(entityId);
		float normalized = progress.value/progress.duration;
		int id = (int)(normalized * (variant.count-1));

		id = MathUtils.clamp(id, 0, variant.count -1);
		// update stuff if progress changed
		if (variant.current != id) {
			variant.current = id;
			assets.load(entityId);
		}
		// last, we are done
		if (variant.current == variant.count - 1) {
			mBuildProgress.remove(entityId);
			// NOTE or replace with new entity or whatever
		}
	}
}
