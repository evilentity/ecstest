package io.piotrjastrzebski.edt.deferred.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.artemis.systems.IteratingSystem;
import io.piotrjastrzebski.edt.deferred.components.*;

/**
 * Created by EvilEntity on 03/12/2015.
 */
public class CullingSystem extends IteratingSystem {
	private ComponentMapper<AABB> mAABB;
	private ComponentMapper<Culled> mCulled;

	@Wire ViewBoundsSystem vb;

	public CullingSystem () {
		super(Aspect.all(Renderable.class, AABB.class));
	}

	@Override protected void process (int entityId) {
		AABB aabb = mAABB.get(entityId);
		// we don't care to be super accurate aabb check is good enough
		// TODO use extra bounds, and check for view in renderer?
		if (aabb.overlaps(vb.getViewBounds())) {
			mCulled.remove(entityId);
		} else {
			mCulled.create(entityId);
		}
	}
}
