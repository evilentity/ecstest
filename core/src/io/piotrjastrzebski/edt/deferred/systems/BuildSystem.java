package io.piotrjastrzebski.edt.deferred.systems;

import com.artemis.*;
import com.artemis.annotations.Wire;
import com.artemis.utils.IntBag;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import io.piotrjastrzebski.edt.Side;
import io.piotrjastrzebski.edt.deferred.components.*;
import io.piotrjastrzebski.edt.deferred.components.assets.DiffuseAssetDef;

/**
 * Created by EvilEntity on 30/11/2015.
 */
public class BuildSystem extends BaseSystem {
	private ComponentMapper<Variant> mVariant;
	private ComponentMapper<ViewDirection> mFacing;
	private ComponentMapper<AABB> mAABB;
	private ComponentMapper<DismantleProgress> mDismantleProgress;
	@Wire CursorSystem cs;

	private Side side = Side.NORTH;
	EntitySubscription sub;
	@Override protected void initialize () {
		 sub = world.getAspectSubscriptionManager().get(Aspect.all(AABB.class));
	}

	float cooldown;
	@Override protected void processSystem () {
		updateSide();
		cooldown -= world.delta;
		if (Gdx.input.isButtonPressed(Input.Buttons.LEFT) && cooldown < 0) {
			cooldown = 1;
			IntBag entities = sub.getEntities();
			for (int i = 0; i < entities.size(); i++) {
				int eid = entities.get(i);
				AABB aabb = mAABB.get(eid);
				if (aabb.contains(cs.x, cs.y)) {
					mDismantleProgress.create(eid).setDuration(1);
					return;
				}
			}
			createBuild((int)cs.x-1, (int)cs.y-1, side);
		}
	}

	private void updateSide () {
		if (Gdx.input.isKeyJustPressed(Input.Keys.SEMICOLON)) {
			side = side.prev();
			Gdx.app.log("", "Side " + side);
		} else if (Gdx.input.isKeyJustPressed(Input.Keys.APOSTROPHE)) {
			side = side.next();
			Gdx.app.log("", "Side " + side);
		}
	}

	private EntityEdit createBuild(float x, float y, Side side) {
		EntityEdit edit = world.edit(world.create());
		edit.create(DiffuseAssetDef.class).setPath("building/build");
		edit.create(Variant.class).setCount(4).setCurrent(0);
		edit.create(ViewDirection.class).set(side);
		edit.create(Transform.class).setPosition(x, y);
		edit.create(Size.class).set(2, 2);
		edit.create(DebugColor.class).set(Color.GREEN);
		edit.create(Renderable.class);
		edit.create(AABB.class);
		edit.create(StaticEntity.class);
		edit.create(BuildProgress.class).setDuration(1);
		return edit;
	}
}
