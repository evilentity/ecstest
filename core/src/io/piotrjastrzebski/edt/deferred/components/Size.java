package io.piotrjastrzebski.edt.deferred.components;

import com.artemis.PooledComponent;

/**
 * Created by EvilEntity on 26/11/2015.
 */
public class Size extends PooledComponent {
	public float width;
	public float height;

	@Override protected void reset () {
		width = height = 0;
	}

	public Size set (float width, float height) {
		this.width = width;
		this.height = height;
		return this;
	}
}
