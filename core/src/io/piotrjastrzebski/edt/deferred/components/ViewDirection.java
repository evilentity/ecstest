package io.piotrjastrzebski.edt.deferred.components;

import com.artemis.PooledComponent;
import io.piotrjastrzebski.edt.Side;

/**
 * Created by EvilEntity on 26/11/2015.
 */
public class ViewDirection extends PooledComponent {
	public Side side;

	@Override protected void reset () {
		side = Side.NORTH;
	}

	public ViewDirection set (Side side) {
		this.side = side;
		return this;
	}
}
