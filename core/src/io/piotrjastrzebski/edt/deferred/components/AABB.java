package io.piotrjastrzebski.edt.deferred.components;

import com.artemis.PooledComponent;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by EvilEntity on 26/11/2015.
 */
public class AABB extends PooledComponent {
	public float x;
	public float y;
	public float width;
	public float height;

	@Override protected void reset () {
		set(0, 0, 0, 0);
	}

	public AABB set(Rectangle rectangle) {
		return set(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
	}

	public AABB set (float x, float y, float width, float height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		return this;
	}

	public boolean overlaps (Rectangle r) {
		return x < r.x + r.width && x + width > r.x && y < r.y + r.height && y + height > r.y;
	}

	public boolean contains (float x, float y) {
		return this.x <= x && this.x + this.width >= x && this.y <= y && this.y + this.height >= y;
	}
}
