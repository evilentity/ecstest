package io.piotrjastrzebski.edt.deferred.components;

import com.artemis.PooledComponent;
import com.badlogic.gdx.graphics.Color;

/**
 * Created by EvilEntity on 26/11/2015.
 */
public class DebugColor extends PooledComponent {
	public Color color = new Color(Color.WHITE);

	public DebugColor set(Color color) {
		this.color.set(color);
		return this;
	}

	public DebugColor set(float r, float g, float b, float a) {
		this.color.set(r, g, b, a);
		return this;
	}

	@Override protected void reset () {
		color.set(Color.WHITE);
	}
}
