package io.piotrjastrzebski.edt.deferred.components;

import com.artemis.PooledComponent;

/**
 * Created by EvilEntity on 26/11/2015.
 */
public class Transform extends PooledComponent {
	public float x;
	public float y;
	public float zOffset;
	public float rotation;

	@Override protected void reset () {
		x = y = zOffset = rotation = 0;
	}

	public Transform setPosition (float x, float y) {
		this.x = x;
		this.y = y;
		return this;
	}

	public Transform setZOffset (float zOffset) {
		this.zOffset = zOffset;
		return this;
	}
	public Transform setRotation (float rotation) {
		this.rotation = rotation;
		return this;
	}
}
