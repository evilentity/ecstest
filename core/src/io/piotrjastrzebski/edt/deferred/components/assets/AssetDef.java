package io.piotrjastrzebski.edt.deferred.components.assets;

import com.artemis.PooledComponent;

/**
 * Created by EvilEntity on 26/11/2015.
 */
public abstract class AssetDef extends PooledComponent {
	public String path;
	public float xOffset;
	public float yOffset;
	public float width;
	public float height;

	public AssetDef () {
		reset();
	}

	@Override protected void reset () {
		path = null;
		xOffset = yOffset = 0;
		width = -1;
		height = -1;
	}

	public AssetDef setPath (String path) {
		this.path = path;
		return this;
	}

	public AssetDef setOffsets(float xOffset, float yOffset) {
		this.xOffset = xOffset;
		this.yOffset = yOffset;
		return this;
	}

	public AssetDef setSize(float width, float height) {
		this.width = width;
		this.height = height;
		return this;
	}
}
