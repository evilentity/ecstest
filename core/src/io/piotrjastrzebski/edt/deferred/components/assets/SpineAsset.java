package io.piotrjastrzebski.edt.deferred.components.assets;

import com.artemis.PooledComponent;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.Skeleton;

/**
 * Created by EvilEntity on 01/12/2015.
 */
public class SpineAsset extends PooledComponent {
	public Skeleton skeleton;
	public AnimationState state;

	@Override protected void reset () {
		skeleton = null;
		state = null;
	}
}
