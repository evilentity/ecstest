package io.piotrjastrzebski.edt.deferred.components.assets;

import com.artemis.PooledComponent;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by EvilEntity on 26/11/2015.
 */
public abstract class Asset extends PooledComponent {
	public TextureRegion region;
	public float xOffset;
	public float yOffset;
	public float width;
	public float height;

	public Asset setRegion(TextureRegion region) {
		this.region = region;
		return this;
	}

	public Asset setOffsets(float xOffset, float yOffset) {
		this.xOffset = xOffset;
		this.yOffset = yOffset;
		return this;
	}

	public Asset setSize(float width, float height) {
		this.width = width;
		this.height = height;
		return this;
	}

	@Override protected void reset () {
		region = null;
		xOffset = yOffset = width = height = 0;
	}
}
