package io.piotrjastrzebski.edt.deferred.components.assets;

import com.artemis.PooledComponent;

/**
 * Created by EvilEntity on 01/12/2015.
 */
public class SpineAssetDef extends PooledComponent {

	public String path;
	public String skin;
	public String anim;
	public boolean loop;
	public float animScale = 1;

	@Override protected void reset () {
		path = null;
		skin = null;
		anim = null;
		animScale = 1;
	}

	public SpineAssetDef setPath (String path) {
		this.path = path;
		return this;
	}

	public SpineAssetDef setAnim (String anim) {
		this.anim = anim;
		return this;
	}

	public SpineAssetDef setSkin (String skin) {
		this.skin = skin;
		return this;
	}

	public SpineAssetDef setLoop (boolean loop) {
		this.loop = loop;
		return this;
	}

	public SpineAssetDef setAnimScale (float animScale) {
		this.animScale = animScale;
		return this;
	}
}
