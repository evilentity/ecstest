package io.piotrjastrzebski.edt.deferred.components;

import com.artemis.PooledComponent;

/**
 * Created by EvilEntity on 30/11/2015.
 */
public class DismantleProgress extends PooledComponent {
	public float value;
	public float duration;

	@Override protected void reset () {
		value = 0;
		duration = 1;
	}

	public void setDuration (float duration) {
		this.duration = duration;
	}
}
