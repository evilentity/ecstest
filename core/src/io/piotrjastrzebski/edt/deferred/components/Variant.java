package io.piotrjastrzebski.edt.deferred.components;

import com.artemis.PooledComponent;
import io.piotrjastrzebski.edt.Side;

/**
 * Created by EvilEntity on 26/11/2015.
 */
public class Variant extends PooledComponent {
	public int current;
	public int count;

	@Override protected void reset () {
		current = 0;
	}

	public Variant setCurrent (int current) {
		this.current = current;
		return this;
	}

	public Variant setCount (int count) {
		this.count = count;
		return this;
	}
}
