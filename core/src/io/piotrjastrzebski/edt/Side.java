package io.piotrjastrzebski.edt;

/**
 * Created by EvilEntity on 30/11/2015.
 */
public enum Side {
	// damn, that weird
	NORTH("-n"), EAST("-e"), SOUTH("-s"), WEST("-w");

	private final int id;
	private final String suffix;
	// values generates new array each time, so we cache it
	private static Side[] values = values();

	Side (String suffix) {
		this.suffix = suffix;
		id = ordinal();
	}

	public String getSuffix () {
		return suffix;
	}

	public Side prev() {
		return (id > 0)?values[id - 1]:values[values.length - 1];
	}

	public Side next() {
		return (id < values.length - 1)?values[id + 1]:values[0];
	}
}
