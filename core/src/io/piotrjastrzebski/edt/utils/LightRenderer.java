package io.piotrjastrzebski.edt.utils;

import box2dLight.Light;
import box2dLight.RayHandler;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Created by EvilEntity on 25/10/2015.
 */
public class LightRenderer {
	public final static String U_LIGHT_POS = "u_lightPos";
	public final static String U_LIGHT_INTENSITY = "u_lightIntensity";
	public final static String U_RESOLUTION = "u_resolution";
	public final static String U_ASPECT_FIX = "u_aspectFix";
	public final static String U_NORMAL_MAP = "u_normals";

	float screenWidth = Gdx.graphics.getWidth();
	float screenHeight = Gdx.graphics.getHeight();

	OrthographicCamera camera;

	ShaderProgram shader;
	public RayHandler rayHandler;

	float lightZ = .25f;
	float lightInt = 2.5f;
	Color ambient = new Color(Color.GRAY);

	public LightRenderer (final OrthographicCamera camera, World world) {
		this.camera = camera;
		// TODO do we need this for anything?
		RayHandler.setGammaCorrection(true);
		RayHandler.useDiffuseLight(true);

		shader = createLightShader();
		if (shader == null) throw new IllegalStateException("Failed to load shader!");

		final Vector3 pos = new Vector3();
		// no need to search for the id each time light is rendered
		final int uLightPos = shader.getUniformLocation(U_LIGHT_POS);
		final int uIntensity = shader.getUniformLocation(U_LIGHT_INTENSITY);
		rayHandler = new RayHandler(world, Gdx.graphics.getWidth(), Gdx.graphics.getHeight()) {
			@Override
			protected void updateLightShader() {}

			@Override
			protected void updateLightShaderPerLight(Light light) {
				// TODO can we make this cleaner?
				// project game position to screen position
				camera.project(pos.set(light.getX(), light.getY(), 0));
				// light position must be normalized
				float x = pos.x / screenWidth;
				float y = pos.y / screenHeight;
				float z = lightZ;
				float intensity = lightInt;
				shader.setUniformf(uLightPos, x, y, z);
				shader.setUniformf(uIntensity, intensity);
			}
		};

		rayHandler.setAmbientLight(ambient);

		rayHandler.setLightShader(shader);
	}

	public void render (Texture normals) {
		normals.bind(1);
		rayHandler.setAmbientLight(ambient);
		rayHandler.setCombinedMatrix(camera);
		rayHandler.updateAndRender();
	}

	public void resize (int width, int height) {
		// update uniforms
		shader.begin();
		shader.setUniformf(U_RESOLUTION, width, height);
		shader.setUniformf(U_ASPECT_FIX, (float)width / (float)height);
		shader.end();
		// re init the handler with new fbo
		rayHandler.resizeFBO(width, height);
	}

	public void dispose () {
		rayHandler.dispose();
		shader.dispose();
	}

	private ShaderProgram createLightShader() {
		// Shader adapted from https://github.com/mattdesl/lwjgl-basics/wiki/ShaderLesson6
		ShaderProgram.pedantic = false;
		ShaderProgram lightShader = new ShaderProgram(Gdx.files.internal("shaders/lights.vsh"), Gdx.files.internal("shaders/lights.fsh"));
		if (!lightShader.isCompiled()) {
			Gdx.app.log("ERROR", "Failed to load light shader " + lightShader.getLog());
			return null;
		}

		lightShader.begin();
		lightShader.setUniformi(U_NORMAL_MAP, 1);
		lightShader.setUniformf(U_RESOLUTION, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		lightShader.setUniformf(U_ASPECT_FIX, (float) Gdx.graphics.getWidth() / (float) Gdx.graphics.getHeight());
		lightShader.end();

		return lightShader;
	}
}
