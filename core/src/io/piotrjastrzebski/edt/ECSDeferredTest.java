package io.piotrjastrzebski.edt;

import com.badlogic.gdx.Game;
import com.kotcrab.vis.ui.VisUI;

public class ECSDeferredTest extends Game {
	
	@Override
	public void create () {
		VisUI.load();
		setScreen(new TestScreen());
	}

	@Override public void dispose () {
		super.dispose();
		VisUI.dispose();
	}
}
