package io.piotrjastrzebski.edt;

import com.artemis.EntityEdit;
import com.artemis.World;
import com.artemis.WorldConfiguration;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import io.piotrjastrzebski.edt.deferred.components.*;
import io.piotrjastrzebski.edt.deferred.components.assets.DiffuseAssetDef;
import io.piotrjastrzebski.edt.deferred.components.assets.NormalAssetDef;
import io.piotrjastrzebski.edt.deferred.components.assets.SpineAssetDef;
import io.piotrjastrzebski.edt.deferred.systems.*;
import io.piotrjastrzebski.edt.deferred.systems.assets.AssetsManager;
import io.piotrjastrzebski.edt.deferred.systems.rendering.*;
import io.piotrjastrzebski.edt.deferred.systems.rendering.GridRenderer;
import io.piotrjastrzebski.edt.utils.LightRenderer;
import io.piotrjastrzebski.edt.utils.NormalBatch;

/**
 * Created by EvilEntity on 26/11/2015.
 */
public class TestScreen extends BaseScreen {
	private World world;
	private com.badlogic.gdx.physics.box2d.World box2d;
	private NormalBatch normalBatch;
	private LightRenderer lightRenderer;
	private PolygonSpriteBatch polyBatch;

	public TestScreen () {
		WorldConfiguration config = new WorldConfiguration();
		box2d = new com.badlogic.gdx.physics.box2d.World(new Vector2(), true);
		config.register(batch);
		config.register(normalBatch = new NormalBatch(1000, createNormalShader()));
		config.register(polyBatch = new PolygonSpriteBatch());
		config.register(gameCamera);
		config.register(gameViewport);
		config.register(renderer);
		config.register(lightRenderer = new LightRenderer(gameCamera, box2d));
		config.setSystem(new AssetsManager());
		config.setSystem(new AABBSystem());
		config.setSystem(new CursorSystem());
		config.setSystem(new ViewBoundsSystem());
		config.setSystem(new CullingSystem());

//		config.setSystem(new BuildProgressSystem());
//		config.setSystem(new SpineBuildProgressSystem());
//		config.setSystem(new DismantleProgressSystem());
//		config.setSystem(new SpineDismantleProgressSystem());

		config.setSystem(new DragSystem());
//		config.setSystem(new BuildSystem());
		config.setSystem(new SpineBuildSystem());
//		config.setSystem(new GridRenderer());
//		config.setSystem(new Rendering());
		config.setSystem(new RendererProcessor());
		config.setSystem(new SpriteRenderer());
		config.setSystem(new SpineRenderer());
		config.setSystem(new NormalSpriteRenderer());
		config.setSystem(new NormalSpineRenderer());

//		config.setSystem(new DebugNormalRenderer());

		config.setSystem(new AABBRenderer());
		config.setSystem(new GridRenderer());
		config.setSystem(new LightsRenderer());
		config.setSystem(new CamBoundsSystem());
		world = new World(config);

		EntityEdit grid = world.createEntity().edit();
		grid.create(DebugGrid.class);
		grid.create(RendererProcessor.Renderable.class);

//		Gdx.app.log("", "Grid id = " + grid.getEntityId());
///*
		for (int i = 0; i < 100; i++) {
			create(
				MathUtils.random(-20, 20),
				MathUtils.random(-15, 15),
				MathUtils.random(1, 4),
				MathUtils.random(1, 4),
//				0, MathUtils.random(0, 360)
				0, 0
			).create(StaticEntity.class);
		}
//		*/
//		create(5, 0, 5, 5, 0, 0).create(StaticEntity.class);

//		createBuild(-1, .5f, Side.NORTH);
//		createBuild(-1, -2.5f, Side.SOUTH);
//		createBuild(-3.5f, -1, Side.WEST);
//		createBuild(1.5f, -1, Side.EAST);

//		createCube(-12, 0);
		createCube(0, 0);
//		createCube(12, 0);

//		EntityEdit main = create(0, 0, 4, 2, 0, 0);
		EntityEdit main = create(0, 0, 0.01f, 0.01f, 0, 0);
		main.create(Drag.class);
		main.create(DynamicEntity.class);
//		RendererProcessor.Renderable renderable = main.create(RendererProcessor.Renderable.class);
//		renderable.setLayer(1);
//		renderable.renderers[0] = 15;
//		renderable.rendererCount = 1;
	}

	private EntityEdit create(float x, float y, float width, float height, float zOffset, float rotation) {
		EntityEdit edit = world.edit(world.create());
		edit.create(DiffuseAssetDef.class).setPath("box64.png");
		edit.create(NormalAssetDef.class).setPath("n-box64.png");
		edit.create(RendererProcessor.Renderable.class).setLayer(1);
//		edit.create(NormalAssetDef.class).setPath("box64.png");
//		edit.create(SpineAssetDef.class).setPath("build/cube.json");
		edit.create(Transform.class).setPosition(x, y).setZOffset(zOffset).setRotation(rotation);
		edit.create(Size.class).set(width, height);
		edit.create(DebugColor.class).set(Color.RED);
		edit.create(Renderable.class);
		edit.create(AABB.class);
		return edit;
	}

	private EntityEdit createCube(float x, float y) {
		EntityEdit edit = world.edit(world.create());
 		edit.create(SpineAssetDef.class).setPath("cube/cube.json").setAnim("idle2").setLoop(true).setSkin("idle").setAnimScale(.25f);
		edit.create(RendererProcessor.Renderable.class).setLayer(1);
		edit.create(Transform.class).setPosition(x -5, y -5);
		edit.create(Size.class).set(10, 10);
		edit.create(DebugColor.class).set(Color.GREEN);
		edit.create(Renderable.class);
		edit.create(AABB.class);
		edit.create(StaticEntity.class);
		return edit;
	}

	private EntityEdit createBuild(float x, float y, Side side) {
		EntityEdit edit = world.edit(world.create());
//		edit.create(DiffuseAssetDef.class).setPath("build/build");
		SpineAssetDef spineAssetDef = edit.create(SpineAssetDef.class).setPath("building/build.json").setAnim("construct");
		switch (side) {
		case NORTH:
			spineAssetDef.setSkin("north");
			break;
		case EAST:
			spineAssetDef.setSkin("east");
			break;
		case SOUTH:
			spineAssetDef.setSkin("south");
			break;
		case WEST:
			spineAssetDef.setSkin("west");
			break;
		}
//		edit.create(Variant.class).setCount(4).setCurrent(0);
		edit.create(ViewDirection.class).set(side);
		edit.create(RendererProcessor.Renderable.class).setLayer(1);;
		edit.create(Transform.class).setPosition(x, y);
		edit.create(Size.class).set(2, 2);
		edit.create(DebugColor.class).set(Color.GREEN);
		edit.create(Renderable.class);
		edit.create(AABB.class);
		edit.create(StaticEntity.class);
		edit.create(BuildProgress.class).setDuration(1);
		return edit;
	}

	private ShaderProgram createNormalShader () {
		ShaderProgram shader = new ShaderProgram(Gdx.files.internal("shaders/normals.vsh"), Gdx.files.internal("shaders/normals.fsh"));
		if (!shader.isCompiled()) {
			Gdx.app.error("", "Failed to reload normal shader: " + shader.getLog());
			return null;
		}
		shader.begin();
		shader.setUniformi("u_normals", 0);
		shader.setUniformi("u_diffuse", 1);
		shader.end();
		return shader;
	}

	@Override public void render (float delta) {
		Gdx.gl.glClearColor(.5f, 0f, 0f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		world.setDelta(delta);
		world.process();
	}

	@Override public void resize (int width, int height) {
		super.resize(width, height);

	}

	@Override public void dispose () {
		super.dispose();
		world.dispose();
		normalBatch.dispose();
		lightRenderer.dispose();
		polyBatch.dispose();
	}
}
